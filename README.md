# Guia de Usuario básica

### Ventana secundaria de bienvenida
Al ejecutar el programa, SSHUp.jar, nos aparecerá la ventana de bienvenida dónde se podemos encontrar dos enlaces de ayuda al usuario.
 
 
### Ventana principal
Esta ventana consta de una barra de menús y tres paneles diferenciados, pestaña de conexión rápida, pestaña de conexión desde fichero y panel de progreso.
Barra de menús
 
En la barra de menú tenemos los siguientes elementos:

*	Archivo > Conexión rápida/Desde fichero: Permiten seleccionar cualquiera de las opciones Fichero/Directorio tanto de la pestaña de conexión rápida como de la conexión desde fichero.
*	Opciones > Tema: Permite cambiar entre distintos “Look & Feel” de java.
*	Opciones > Log: Scroll abajo:  Permite habilitar/deshabilitar la opción para hacer foco a la última línea escrita en la ventana secundaria de logs.
*	Opciones > Dibujos: Abre la ventana secundaria con espacio para dibujar.
*	Ayuda > Guía de usuario/Documentación: Abre enlaces hacia la guía de usuario/documentación del proyecto en BitBucket.
*	Ayuda > Acerca de SSHUp: Muestra un diálogo con la información de la versión, UO y nombre del creador de la aplicación.

### Pestaña de conexión rápida
Esta pestaña muestra una interfaz de “conexión rápida” en la que insertaremos todos los equipos vía teclado.


1. A través del campo de texto Equipo agregaremos una o varias direcciones IP en el cuadro lista.
2. El botón Añadir sólo será accionable tras haber introducido una dirección IP válida en el campo Equipo. En ese momento aparecerá el check  en verde. También es posible agregar elementos a la lista utilizando la tecla ENTER del teclado.
3.	Una vez existan elementos en la lista, los botones Borrar y Borrar todos estarán habilitados. El botón borrar borrará el elemento seleccionado, mientras que borrar todos, eliminará toda la lista. Ambos botones disponen de una comprobación en caso de haberse clicado por error, la cual solicita confirmación para realizar la acción.
4.	Los campos usuario y contraseña sólo son editables desde el diálogo modal “Credenciales”. Una vez clicado el botón Credenciales, aparecerá un diálogo solicitando los campos. Al aceptar se mostrarán en la ventana principal.
5.	Este componente permite seleccionar entre Ficheros o Directorios. Al seleccionar el botón, se habilitará la fila correspondiente de elementos. Los botones Buscar fichero y Buscar carpeta abren un diálogo predefinido dónde podremos seleccionar el fichero o carpeta a utilizar.
 
### Pestaña conexión desde fichero ###
 

Esta pestaña muestra una interfaz de “conexión vía fichero”.

1. Utilizando el botón importar, cargamos un archivo con el formato adecuado para insertar datos en el árbol. Por ejemplo: `Ubicacion1;192.168.15.150;192.168.15.11`
2. El botón borrar permite borrar por completo el árbol.
3. Los campos usuario y contraseña sólo son editables desde el diálogo modal “Credenciales”. Una vez clicado el botón Credenciales, aparecerá un diálogo solicitando los campos. Al aceptar se mostrarán en la ventana principal.
4. Este componente permite seleccionar entre Ficheros o Directorios. Al seleccionar el botón, se habilitará la fila correspondiente de elementos. Los botones Buscar fichero y Buscar carpeta abren un diálogo predefinido dónde podremos seleccionar el fichero o carpeta a utilizar. 

 
### Panel de progreso ###
 
Este panel se habilita una vez se hayan introducido datos en alguna de las pestañas anteriormente descritas.

* Barra de progreso: muestra el porcentaje de progreso de la transferencia a cada equipo.
* Iniciar: lanza el proceso.
* Cancelar: para el proceso.
* Ver log: muestra u oculta la ventana secundaria de log con información de la transferencia. Sólo puede ser accionado cuando el proceso se encuentra iniciado.
* En espera…: muestra información sobre el estado de la transferencia.
 
### Ventana secundaria de logs ###
 
Esta ventana recibe información relevante al proceso ejecutado. 

*	Tiempo transcurrido de la conexión al equipo actual.
*	Tiempo estimado restante de la conexión al equipo actual.
*	Usuario, equipo y estado de la transferencia.

Además, también muestra las coordenadas actuales del ratón cuando éste se encuentra en la ventana secundaria de dibujo.
Dispone de un botón   que activa/desactiva la función de hacer foco en la última línea del log.
 
### Ventana secundaria para dibujar ###
  
Permite dibujar un óvalo en la superficie de dibujo. Están implementados los siguientes eventos:

+	Mouse clic: Genera un círculo utilizando la posición del ratón como centro.
     *	Clic izquierdo: El círculo generado está relleno y es de color amarillo.
     *	Clic derecho: El círculo generado no está relleno y es de color naranja.
     *	Clic central (rueda ratón): Establece el último color elegido en el menú Formato > Color o en caso de no haber seleccionado ninguno, el por defecto (negro).
-	Mouse move: El elemento dibujado se moverá utilizando la posición del ratón como centro.
*	Mouse clic & drag: Permite crear óvalos del tamaño del desplazamiento realizado con el ratón.
### Requisitos mínimos ###
*	Java 8 o superior.
